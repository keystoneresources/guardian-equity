$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
	      mobilePressColumns();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    }
	  }, 
	  {
	    context: 'range_1',
	    match: function() {
	      //responsiveEqualHeight();\
	      slideDrawerEnable();
	      mobilePressColumns();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
	      desktopPressColumns();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      slideDrawerDisable();
	      desktopPressColumns();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      slideDrawerDisable();
	      desktopPressColumns();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      slideDrawerDisable();
	      desktopPressColumns();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	]; 
	MQ.init(queries);
	var range = MQ.new_context;
	
	if($('body.page-9').length){ 
	
		// Coordinate generator
		$('#markers').click(function(e) {
	    var xcoord = e.offsetX/ $(this).width() * 100;
	    var ycoord = e.offsetY/ $(this).height() * 100;
	    $('.marker').remove();
	    $(this).append('<div class="marker" style="left:'+xcoord+'%; top: '+ycoord+'%;"></div>');
			$('h1').html("xcoord is "+xcoord+" and ycoord is "+ycoord);
	  });  
	} 
	
	if($('body.type-6').length){
			var itemAmount = $('#giving-back .col-lg-6.col-md-6.col-ms-6.col-sm-6.col-xs-12.col-xs-height').length;
			if(itemAmount % 2 != 0){
				$('#giving-back .row').append('<div class="col-lg-6 col-md-6 col-ms-6 col-sm-6 col-xs-12 col-xs-height empty"></div>');
			}
			$('.row-same-height > .col-lg-6.col-md-6.col-ms-6.col-sm-6.col-xs-12.col-xs-height').unwrap();
			var $children = $('#giving-back .col-lg-6');
	    for(var i = 0, l = $children.length; i < l; i += 2) {
	      $children.slice(i, i+2).wrapAll('<div class="row-same-height"></div>');
	    }
		}
	
	function desktopPressColumns(){
		if($('body.type-4').length){
			var itemAmount = $('#press-media .col-lg-4.col-md-4.col-ms-4.col-sm-12.col-xs-12.col-xs-height').length;
			var newItemAmount = itemAmount + 1;
			if(itemAmount % 3 != 0 && newItemAmount % 3 != 0){
				$('#press-media .row').append('<div class="col-lg-4 col-md-4 col-ms-4 col-sm-12 col-xs-12 col-xs-height empty"></div>');
				$('#press-media .row').append('<div class="col-lg-4 col-md-4 col-ms-4 col-sm-12 col-xs-12 col-xs-height empty"></div>');
			}else if(itemAmount % 3 != 0 && newItemAmount % 3 === 0){
				$('#press-media .row').append('<div class="col-lg-4 col-md-4 col-ms-4 col-sm-12 col-xs-12 col-xs-height empty"></div>');
			}
			$('.row-same-height > .col-lg-4.col-md-4.col-ms-4.col-sm-12.col-xs-12.col-xs-height').unwrap();
			var $children = $('#press-media .col-lg-4');
	    for(var i = 0, l = $children.length; i < l; i += 3) {
	      $children.slice(i, i+3).wrapAll('<div class="row-same-height"></div>');
	    }
		}
	}
	
	function mobilePressColumns(){
		if($('body.type-4').length){
			var itemAmount = $('#press-media .col-lg-4.col-md-4.col-ms-4.col-sm-12.col-xs-12.col-xs-height').length;
			if(itemAmount % 2 != 0){
				$('#press-media .row').append('<div class="col-lg-4 col-md-4 col-ms-4 col-sm-12 col-xs-12 col-xs-height empty"></div>');
			}
			$('.row-same-height > .col-lg-4.col-md-4.col-ms-4.col-sm-12.col-xs-12.col-xs-height').unwrap();
			var $children = $('#press-media .col-lg-4');
	    for(var i = 0, l = $children.length; i < l; i += 2) {
	      $children.slice(i, i+2).wrapAll('<div class="row-same-height"></div>');
	    }
		}
	}
	

  function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    var drawer = '.snap-drawer-';
    //var contentBorder = '#content_push';
    $('.snap-drawers').show();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '200px'}, function(){
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-200px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//snapDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.snap-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }


}); //End Document Ready